package storage

// Storage interface
type Storage interface {
	Get(key string) Object
	Save(object Object) string
	Delete(key string) bool
	List(keys []string) []Object
}

// Object used as wrapper around data
type Object struct {
	Name string
	Data []byte
}

func (st Object) String() string {
	return "{ Name: " + st.Name + " - " + "Data: " + string(st.Data) + " }"
}
