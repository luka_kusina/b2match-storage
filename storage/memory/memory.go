package memory

import (
	"fmt"
	"time"

	"b2match.io/luka-storage/storage"
)

type Memory struct {
	StorageMap map[string]storage.Object
}

// Get object out of memory
func (m Memory) Get(key string) storage.Object {
	fmt.Printf("fetching data with key: %v \n", key)
	v, ok := m.StorageMap[key]
	if ok {
		return v
	}
	return storage.Object{}
}

// Save object to memory
func (m Memory) Save(data storage.Object) string {
	fmt.Println("Saving data and assigning key!")
	currentTime := time.Now()
	generatedName := currentTime.Format("2006-01-02T15:04:05.999999-07:00") + "-" + data.Name
	m.StorageMap[generatedName] = data
	return generatedName
}

// Delete object from memory
func (m Memory) Delete(key string) bool {
	_, ok := m.StorageMap[key]
	if ok {
		fmt.Println("Deleting object with key: " + key)
		delete(m.StorageMap, key)
	}
	return ok
}

// List objects from memory
func (m Memory) List(keys []string) []storage.Object {
	objects := make([]storage.Object, 0)
	for _, v := range keys {
		_, ok := m.StorageMap[v]
		if ok {
			objects = append(objects, m.StorageMap[v])
		}
	}
	return objects
}
