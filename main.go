package main

import (
	"fmt"
	"io/ioutil"

	"b2match.io/luka-storage/storage"

	"b2match.io/luka-storage/storage/memory"
)

/* // TestStruct - structure used for testing
type TestStruct struct {
	Name          string
	Number        int
	BoolStatement bool
}

func (t TestStruct) String() string {
	return fmt.Sprintf("{ Name: %v Number: %v BoolStatement: %v}", t.Name, t.Number, t.BoolStatement)
} */

func main() {
	var sto storage.Storage = memory.Memory{StorageMap: make(map[string]storage.Object)}

	dat1, err := ioutil.ReadFile("/Users/lukakusina/test.txt")
	if err != nil {
		panic(err)
	}

	dat2, err := ioutil.ReadFile("/Users/lukakusina/test2.txt")
	if err != nil {
		panic(err)
	}

	dat3, err := ioutil.ReadFile("/Users/lukakusina/Downloads/upisni_obrazac_4_0036502294.pdf")
	if err != nil {
		panic(err)
	}

	storObj1 := storage.Object{Name: "test", Data: dat1}
	storObj2 := storage.Object{Name: "test2", Data: dat2}
	storObj3 := storage.Object{Name: "obrazac", Data: dat3}

	// Saving objects
	fmt.Println("Trying to save objects!")
	fmt.Println()

	generatedKey1 := sto.Save(storObj1)
	fmt.Println("Object is saved! Generated key is " + generatedKey1)
	generatedKey2 := sto.Save(storObj2)
	fmt.Println("Object is saved! Generated key is " + generatedKey2)
	generatedKey3 := sto.Save(storObj3)

	keys := make([]string, 0)
	keys = append(keys, generatedKey1)
	keys = append(keys, generatedKey2)

	fmt.Println()

	//sto.Delete(generatedKey2)

	// Getting objects
	fmt.Println("Trying to get saved data")
	fmt.Println()

	fetchedObject1 := sto.Get(generatedKey1)
	fmt.Println("Fetched object : " + fetchedObject1.String())
	fetchedObject2 := sto.Get(generatedKey2)
	fmt.Println("Fetched object : " + fetchedObject2.String())
	fetchedObject3 := sto.Get(generatedKey3)
	//fmt.Println("Fetched object : " + fetchedObject3.String())

	fmt.Println("Trying to write new pdf file")
	err = ioutil.WriteFile("/Users/lukakusina/"+fetchedObject3.Name, fetchedObject3.Data, 0644)
	if err != nil {
		fmt.Println("ERRROR while writing file")
	}

	fmt.Println()

	fmt.Println("Trying to fetch list!")
	fmt.Println()

	objects := sto.List(keys)
	fmt.Println("Fetched objects")
	for _, v := range objects {
		fmt.Print("   " + v.String() + "\n")
	}

}
